package com.applicationgod.nodejs_api;

/**
 * Created by Garth Toland on 26/05/2016.
 * Description:
 */
public class Blog {

    private int id;
    private String title;
    private String subheading;
    private String body;
    private String author;

    public Blog() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubheading() {
        return subheading;
    }

    public void setSubheading(String subheading) {
        this.subheading = subheading;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }






}
