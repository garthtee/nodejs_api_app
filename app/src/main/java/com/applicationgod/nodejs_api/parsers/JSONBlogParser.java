package com.applicationgod.nodejs_api.parsers;

import com.applicationgod.nodejs_api.Blog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Garth Toland on 26/05/2016.
 * Description: Parses JSON Blog data into blog objects.
 */
public class JSONBlogParser {

    public static Blog parseData(String content) {

        try {
            JSONArray array = new JSONArray(content);
            Blog blog = new Blog();

            for (int i = 0; i < array.length(); i++) {

                JSONObject object = array.getJSONObject(i);

                blog.setId(object.getInt("id"));
                blog.setTitle(object.getString("blogtitle"));
                blog.setSubheading(object.getString("blogsubheading"));
                blog.setBody(object.getString("blogbody"));
                blog.setAuthor(object.getString("blogauthor"));
            }

            return blog;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}
