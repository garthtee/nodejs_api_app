package com.applicationgod.nodejs_api;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Garth Toland on 23/05/2016.
 * Description: Using the URL supplied as a parameter,
 * the getBlogData method retrieves information from a NodeJS API.
 */
public class HttpManager {

    public static String getBlogData(URL url) {

        BufferedReader reader = null;

        try {
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");

            if (connection.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + connection.getResponseCode());
            }

            String string = "";
            reader = new BufferedReader(new InputStreamReader(
                    (connection.getInputStream())));

            String line;
            while ((line = reader.readLine()) != null) {
                string += line;
            }

            connection.disconnect();
            return string;

        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
}
