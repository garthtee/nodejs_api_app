package com.applicationgod.nodejs_api;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.applicationgod.nodejs_api.parsers.JSONBlogParser;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private EditText editText;
    private ArrayList<GET_BLOG_TASK> tasks;
    private ProgressBar progressBar;
    private Blog blog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Defining a milkshake animation //
        final Animation myAnim = AnimationUtils.loadAnimation(this, R.anim.inyourface);


        editText = (EditText) findViewById(R.id.editText);
        Button button = (Button) findViewById(R.id.button);
        button.setAnimation(myAnim);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        try {
            progressBar.setVisibility(View.INVISIBLE);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        tasks = new ArrayList<>();

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(myAnim);
             int blog_id;
             if(!editText.getText().toString().equals("")) {
                 try {
                     blog_id = Integer.valueOf(editText.getText().toString());
                 } catch (NumberFormatException e) {
                    e.printStackTrace();
                     return;
                 }

                 new GET_BLOG_TASK().execute(blog_id);
                 if(blog!=null) {
                     BlogDialog blogDialog = new BlogDialog(MainActivity.this, blog);
                     blogDialog.show();
                 }
             } else {
                 Toast.makeText(getApplicationContext(), "Enter a Blog ID", Toast.LENGTH_SHORT).show();
             }
            }
        });
    }

    private class GET_BLOG_TASK extends AsyncTask<Integer, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (tasks.size() == 0) {
                progressBar.setVisibility(View.VISIBLE);
            }
            tasks.add(this);
        }

        @Override
        protected String doInBackground(Integer... params) {
            String result = "";
            try {
                String urlString = "http://applicationgod.com:3000/getBlogById/";
                urlString += params[0];
                URL url = new URL(urlString);
                result = HttpManager.getBlogData(url);
                blog = new Blog();
                blog = JSONBlogParser.parseData(result);

            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            tasks.remove(this);
            if (tasks.size() == 0) {
                progressBar.setVisibility(View.INVISIBLE);
            }
        }
    }
}