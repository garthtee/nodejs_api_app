package com.applicationgod.nodejs_api;

import android.app.Activity;
import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DialogTitle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class BlogDialog extends Dialog implements View.OnClickListener{

    public Activity activity;
    private Blog blog;
    public Button btnOkay;
    private TextView txtTitle, txtSubheading, txtBody, txtAuthor;

    public BlogDialog(Activity activity, Blog blog) {
        super(activity);
        this.activity = activity;
        this.blog = blog;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_blog_dialog);

        txtTitle = (TextView) findViewById(R.id.txtTitle);
        txtSubheading = (TextView) findViewById(R.id.txtSubheading);
        txtBody = (TextView) findViewById(R.id.txtBody);
        txtAuthor = (TextView) findViewById(R.id.txtAuthor);
        btnOkay = (Button) findViewById(R.id.btnOkay);
        btnOkay.setOnClickListener(this);
        final String mimeType = "text/html";
        final String encoding = "UTF-8";

        txtTitle.setText(blog.getTitle());
        txtSubheading.setText(blog.getSubheading());
        WebView webView = (WebView) findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadData(blog.getBody(), mimeType, encoding);
        txtAuthor.setText(blog.getAuthor());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnOkay:
                dismiss();
                break;
            default:
                break;
        }
        dismiss();
    }
}
